Highcharts.chart('container1', {
    chart: {
        type: 'bar'
    },
    title: {
        text: 'Ticket bar chart'
    },
    xAxis: {
        categories: ['']
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Total ticket consumption'
        }
    },
    legend: {
        reversed: true
    },
    plotOptions: {
        series: {
            stacking: 'normal'
        }
    },
    series: [{
        name: 'Businees',
        data: [5, 3, 4, 7, 2]
    }, {
        name: 'Technical',
        data: [2, 2, 3, 2, 1]
    }, {
        name: 'Other',
        data: [3, 4, 4, 2, 5]
    }]
});