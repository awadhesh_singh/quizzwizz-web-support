<?php

namespace App\Http\Controllers;

use App\CommentAttachment;
use App\User;
use App\Ticket;
use App\Comment;
use App\Http\Requests;
use App\Mailers\AppMailer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentsController extends Controller
{
    protected $attachments=null;
	/**
	 * Persist comment and mail user
	 * @param  Request  $request
	 * @param  AppMailer $mailer
	 * @return Response
	 */
    public function postComment(Request $request, AppMailer $mailer)
    {

    	$this->validate($request, [
            'comment'   => 'required'
        ]);

        $comment = Comment::create([
        	'ticket_id' => $request->input('ticket_id'),
        	'user_id'	=> Auth::user()->id,
        	'comment'	=> $request->input('comment'),
        ]);

        if($request->hasFile('attachment'))
        {
            $this->attachments=  $request->attachment;
            foreach($this->attachments as $k => $v){
                $ext = $v->getClientOriginalExtension();
                $name = $comment->id.uniqid().'.'.$ext;
                $file = base_path().'/attachment/'.$name;
                if(\File::exists($file)){

                    \File::delete($file);

                }
                $v->move(public_path('/attachment/'), $file);

                $commentAttachment = new CommentAttachment();
                $commentAttachment->comment_id =  $comment->id;
                $commentAttachment->attachment =  $name;
                $commentAttachment->save();
            }

        }



        // send mail if the user commenting is not the ticket owner
        if ($comment->ticket->user->id !== Auth::user()->id) {
        	$mailer->sendTicketComments($comment->ticket->user, Auth::user(), $comment->ticket, $comment, $this->attachments);
        }
        
        return redirect()->back()->with("status", "Your comment has be submitted.");
    }
}
