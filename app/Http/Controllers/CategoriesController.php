<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Category;

class CategoriesController extends Controller
{
    public function index()
    {

        $quizCategories = Category::all();

        return view('ticket_categories.index', compact('quizCategories'));
    }

    public function create()
    {
        return view('ticket_categories.ticket_category.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name'   => 'required',
        ]);

        $category = new Category([
            'name'     => $request->input('name'),
        ]);
    
        $category->save();

        return redirect()->back()->with("status", "Category Created!");
    }
}
