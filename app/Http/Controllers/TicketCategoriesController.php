<?php

namespace App\Http\Controllers;

use App\TicketCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;
use Validator;

class TicketCategoriesController extends Controller
{

    /**
     * Display a listing of the quiz categories.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $quizCategories = TicketCategory::get();

        return view('ticket_categories.index', compact('quizCategories'));
    }

    /**
     * Show the form for creating a new quiz category.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        
        
        return view('ticket_categories.create');
    }

    /**
     * Store a new quiz category in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
            $data = ['name'=>$request->name];
//            $data = $this->getData($request);

            TicketCategory::create($data);

            return redirect()->route('ticket_categories.ticket_category.index')
                             ->with('success_message', 'Ticket Category was successfully added!');

        } catch (Exception $exception) {
            $error_messages = ['unexpected_error' => 'Unexpected error occurred while trying to process your request!'];
             if(@$exception->validator){
                    $error_messages = $exception->validator;
              }
            return back()->withInput()
                         ->withErrors($error_messages);
        }
    }

    /**
     * Display the specified quiz category.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $quizCategory = TicketCategory::findOrFail($id);

        return view('ticket_categories.show', compact('quizCategory'));
    }

    /**
     * Show the form for editing the specified quiz category.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $quizCategory = TicketCategory::findOrFail($id);
        

        return view('ticket_categories.edit', compact('quizCategory'));
    }

    /**
     * Update the specified quiz category in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {



            $quizCategory = TicketCategory::findOrFail($id);
            $quizCategory->update(['name'=>$request->name]);

            return redirect()->route('ticket_categories.ticket_category.index')
                             ->with('success_message', 'Ticket Category was successfully updated!');

        } catch (Exception $exception) {
            $error_messages = ['unexpected_error' => 'Unexpected error occurred while trying to process your request!'];
             if($exception->validator){
                    $error_messages = $exception->validator;
              }
            return back()->withInput()
                         ->withErrors($error_messages);
        }
    }

    /**
     * Remove the specified quiz category from the storage.
     *
     * @param  int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $quizCategory = TicketCategory::findOrFail($id);
            $quizCategory->delete();

            return redirect()->route('ticket_categories.ticket_category.index')
                             ->with('success_message', 'Ticket Category was successfully deleted!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'name' => 'string|min:1|max:255'
     
        ];

        $validator = Validator::make($request->all(),$rules);

        return $validator->fails();

        //$data = $this->validate($request, $rules);

        //return $validator;
    }

    public static function getTicketCategory(){
        $quizCategorys = TicketCategory::select('id','name')
            ->get();

        return $quizCategorys;
    }


    /*
      @Description: This code update status
      @Status: done
      @return  json
      @Created By Manjeet Kumar
      @Created Date: 7-11-2019
     @Created Date: 7-11-2019
   */


//    public function updateAjax($id, Request $request)
//    {
//        $type = $request->get('type');
//        $value = $request->get('value');
//        $value = strtolower($value)=='true' ?'true':'false';
//
//        $add['published'] = @$value;
//        $data['updated_at'] = date('Y-m-d H:i:s');
//        QuizCategory::where('id','=',$id)->update($add);
//
//        $message ='Quiz category unpublished successfully.';
//        if($value=='true')
//        {
//            $message ='Quiz category published successfully.';
//        }
//        return ['status'=> 'success', 'message'=> $message];
//    }

}
