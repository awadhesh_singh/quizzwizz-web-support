<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Controllers\Controller;
use App\Ticket;
use App\User;
use Illuminate\Support\Facades\Auth;
use Redirect;
use Schema;
use App\Rules\ValidateAdminExist;
use Illuminate\Http\Request;




class AdminController extends Controller {

	/**
	 * Display a listing of tmpcountries
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index(Request $request)
    {

	}

	/**
	 * Show the form for creating a new tmpcountries
	 *
     * @return \Illuminate\View\View
	 */
    public function showLogin(Request $request)
    {
        if($request->has('user'))
        {
            $data = base64_decode($request->user);
            $id = explode('_',$data)[0];
            if($id>0){
                $user = User::find($id);
                $auth = \Auth::loginUsingId($user->id, $request->get('remember_me'));
            }
            else{
               Auth::logout();
                return redirect()->route('admin.login');
            }
        }

        if (\Auth::check()) {
            return redirect('admin/dashboard');
        }
        return view('admin.login');
    }

    public function showRegister()
    {
        return view('admin.register');
    }

    public function showDashboard()
    {
        $user = \Auth::user();
        $tickets = Ticket::where('user_id', Auth::user()->id)->where('status', 'open')->latest()->take(5)->get();
        $categories = Category::all();
        if(!$user->is_admin)
        {
            $totalTickets = Ticket::where('user_id',$user->id)->count();
            $closedTickets = Ticket::where('user_id',$user->id)->where('status','=','Closed')->count();
            $openTickets = Ticket::where('user_id',$user->id)->where('status','=','Open')->count();
            return view('admin.dashboard', compact( 'openTickets','closedTickets','totalTickets','user', 'tickets', 'categories'));
        }
        $totalTickets = Ticket::count();
        $closedTickets = Ticket::where('status','=','Closed')->count();
        $openTickets = Ticket::where('status','=','Open')->count();
        $categoryCount = Category::count();


        return view('admin.dashboard', compact('categoryCount', 'openTickets','closedTickets','totalTickets','user', 'tickets', 'categories'));
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => ['required', 'email'],
            'password' => 'required',
        ]);

        $user = User::where('email', $request->get('email'))->first();
        if(!$user)
        {
            return redirect()->back()->withInput()->withErrors(['error' => "Invalid user."]);
        }
        $passwordChecked = \Hash::check($request->get('password'), $user->password);
//        if($user->account_status != user::STATUS_ACTIVE){
//            return redirect()->back()->withInput()->withErrors(['password' => "Email is not activated"]);
//        }
      if(!$passwordChecked){
            return redirect()->back()->withInput()->withErrors(['password' => "Invalid login credential."]);
        }

        $auth = \Auth::loginUsingId($user->id, $request->get('remember_me'));
        return redirect('customer/dashboard');

    }

    public function logout(){
       \Auth::logout();
        return redirect('login');
    }

    public function showResetPassword()
    {
        $user = \Auth::user();
        return view('admin.reset_password', compact('user'));
    }

    public function resetPassword(Request $request)
    {

        $user = User::findOrFail($request->user_id);

        $request->validate([
            'password' => 'nullable|required_with:password_confirmation|string|confirmed',
        ]);

        $user->password =  \Hash::make($request->password);
        $user->save();
        return redirect()->route('admin.resetpassword')->with('message', 'Password updated successfully');


    }

}
