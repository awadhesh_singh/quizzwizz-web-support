<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::auth();
Route::get('/register', 'AdminController@showRegister')->name('admin.register');
Route::post('/register', 'Auth\AuthController@register')->name('admin.register');
Route::get('/', 'AdminController@showLogin')->name('admin.login');
Route::get('login', 'AdminController@showLogin')->name('admin.login');
Route::post('login', 'AdminController@login')->name('admin.login');
Route::get('logout', 'AdminController@logout')->name('admin.logout');
//Route::get('admin/dashboard', 'AdminController@showDashboard')->name('admin.dashboard')->middleware(['auth','admin']);






Route::group(['prefix' => 'customer', 'middleware' => ['auth', 'admin']], function() {
    Route::get('dashboard', 'AdminController@showDashboard');
    Route::get('my_tickets', 'TicketsController@userTickets');
    Route::get('new_ticket', 'TicketsController@create');
    Route::post('new_ticket', 'TicketsController@store');
    Route::get('tickets/{ticket_id}', 'TicketsController@show');
});


Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'admin']], function() {
    Route::get('dashboard', 'AdminController@showDashboard');
	Route::get('tickets', 'TicketsController@index');
    Route::get('new_ticket', 'TicketsController@create');
    Route::post('new_ticket', 'TicketsController@store');
	Route::post('close_ticket/{ticket_id}', 'TicketsController@close');
    Route::get('tickets/{ticket_id}', 'TicketsController@show');
    Route::group(
        [
            'prefix' => 'ticket_categories',
        ], function () {

        Route::get('/', 'TicketCategoriesController@index')
            ->name('ticket_categories.ticket_category.index');

        Route::get('/create','TicketCategoriesController@create')
            ->name('ticket_categories.ticket_category.create');

        Route::get('/show/{ticketCategory}','TicketCategoriesController@show')
            ->name('ticket_categories.ticket_category.show')
            ->where('id', '[0-9]+');

        Route::get('/updatestatus/{id}','TicketCategoriesController@updateAjax')->where('id', '[0-9]+');

        Route::get('/{ticketCategory}/edit','TicketCategoriesController@edit')
            ->name('ticket_categories.ticket_category.edit')
            ->where('id', '[0-9]+');

        Route::post('/', 'TicketCategoriesController@store')
            ->name('ticket_categories.ticket_category.store');

        Route::put('ticket_category/{ticketCategory}', 'TicketCategoriesController@update')
            ->name('ticket_categories.ticket_category.update')
            ->where('id', '[0-9]+');

        Route::delete('/ticket_category/{ticketCategory}','TicketCategoriesController@destroy')
            ->name('ticket_categories.ticket_category.destroy')
            ->where('id', '[0-9]+');



    });
});


Route::post('comment', 'CommentsController@postComment');
