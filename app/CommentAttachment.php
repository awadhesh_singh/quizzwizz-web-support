<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentAttachment extends Model
{
	/**
	 * The attributes that are mass assignable.
	 * 
	 * @var  array
	 */

	protected $table = 'comment_attachment';
	protected $fillable = [
		'comment_id', 'attachment'
	];

}
