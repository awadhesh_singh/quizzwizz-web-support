<?php
/*
php artisan resource-file:create Timezones --fields=id,zone,gmt,country_code,published,sort
php artisan resource-file:create BusinessCategories --fields=id,title,parent_id,published,context_id,sort
php artisan resource-file:create JobCategories --fields=id,title,parent_id,published,context_id,sort
php artisan resource-file:create JobTitles --fields=id,title,context_id
php artisan resource-file:create Roles --fields=id,title,alias,section

php artisan resource-file:create JobCategoryTitles --fields=id,job_category_id,job_title_id
php artisan resource-file:create SysSettings --fields=id,key,value,type,description,published

php artisan create:resources Roles --force --without-timestamps --template-name=ajaxified
php artisan create:resources Roles --force --template-name=ajaxified


php artisan resource-file:create Skills --fields=id,title,published,context_id
php artisan create:resources Skills --force --without-timestamps --template-name=ajaxified

php artisan resource-file:create SysSettings --fields=id,key,value,published,type,description,created_at,updated_at
php artisan create:resources Skills --force --without-timestamps --template-name=ajaxified



php artisan resource-file:create Categories --fields="id,title,name:published;html-type:select;options:true|false,created_at,updated_at" --force
php artisan create:resources Category --force --without-timestamps --template-name=ajaxified

php artisan resource-file:create Posts --fields="id,category_id,title,name:description;html-type:textarea,name:img_src;html-type:file,name:published;html-type:select;options:0|1,name:display_type;html-type:select;options:main|brief|unspecified,published_at,created_by,created_at,updated_at" --force
php artisan create:resources Post --force --without-timestamps --template-name=ajaxified


php artisan resource-file:create ContactusQueries --fields="id,name,email,phone,subject,name:details;html-type:textarea,created_at,updated_at" --force
php artisan create:resources ContactusQuery --force --template-name=ajaxified

php artisan resource-file:create QuizCategories --fields="id,title,alias,name:published;html-type:select;options:true|false,created_at,updated_at" --force
php artisan create:resources QuizzCategory --force --template-name=ajaxified

php artisan resource-file:create QuizTypes --fields="id,title,alias,name:is_active;html-type:select;options:1|0,created_at,updated_at" --force
php artisan create:resources QuizType --force --template-name=ajaxified

php artisan resource-file:create Countries --fields="id,name,country_code,name:country_flag;html-type:file, name:sort_order;html-type:number;min:1;max:100,name:status;html-type:select;options:active|inactive,created_at,updated_at" --force
php artisan create:resources Country --force --template-name=ajaxified

php artisan resource-file:create Languages --fields="id,title,name:code;html-type:text,name:status;html-type:select;options:active|inactive,created_at,updated_at" --force
php artisan create:resources Language --force --template-name=ajaxified

php artisan resource-file:create ModuleMasters --fields="id,module_name,name:status;html-type:select;options:active|inactive,created_by,created_at,updated_at" --force
php artisan create:resources ModuleMaster --force --template-name=ajaxified

php artisan resource-file:create LanguageTranslations --fields="id,name:module_name;html-type:select,name:language_id;html-type:select,label_key,label_value,name:status;html-type:select;options:active|inactive,created_by,created_at,updated_at" --force
php artisan create:resources LanguageTranslation --force --template-name=ajaxified

php artisan resource-file:create LabelKeys --fields="id,name:module_master_id;html-type:select,label_key,name:status;html-type:select;options:active|inactive,created_by,created_at,updated_at" --force
php artisan create:resources LabelKey --force --template-name=ajaxified

php artisan resource-file:create ConfigConstants --fields=id,constant_key,constant_value,description,created_at,updated_at
php artisan create:resources ConfigConstant --force --template-name=ajaxified

php artisan resource-file:create ContactUs --fields=id,name,name:email;html-type:text,name:subject;html-type:text;name:message;html-type:textarea,created_at,updated_at
php artisan create:resources ContactUs --force --template-name=ajaxified

php artisan resource-file:create Queries --fields=id,name,name:email;html-type:text,name:subject;html-type:text;name:message;html-type:textarea,name:data;html-type:textarea,created_at,updated_at
php artisan create:resources Query --force --template-name=ajaxified

php artisan resource-file:create Timezones --fields=id,name,name:country_code;html-type:text,name:zone_name;html-type:text;created_at,updated_at
php artisan create:resources Timezone --force --template-name=ajaxified

php artisan resource-file:create SensorshipCategories --fields="id,name,name:published;html-type:select;options:true|false,created_at,updated_at" --force
php artisan create:resources SensorshipCategory --force --template-name=ajaxified

php artisan resource-file:create SensorshipTypes --fields="id,name,name:published;html-type:select;options:true|false,created_at,updated_at" --force
php artisan create:resources SensorshipType --force --template-name=ajaxified

php artisan resource-file:create Testimonials --fields="id,name,name:desctiption;html-type:textarea,name:image;html-type:file,created_at,updated_at" --force
php artisan create:resources Testimonial --force --template-name=ajaxified

php artisan resource-file:create QuestionTypes --fields="id,name:title;html-type:text,name:alias;html-type:text,name:published;html-type:select;options:true|false,created_at,updated_at" --force
php artisan create:resources QuestionType --force --template-name=ajaxified

php artisan resource-file:create QuestionHintTypes --fields="id,name:title;html-type:text,name:hint_alias;html-type:text,name:published;html-type:select;options:true|false,created_at,updated_at" --force
php artisan create:resources QuestionHintType --force --template-name=ajaxified

php artisan resource-file:create Questions --fields="id,name:ques_type_id;html-type:select;options:QuestionType,name:ques_hint_type;html-type:select;options:QuestionHintType,name:ques_hint;html-type:text,name:description;html-type:textarea,name:opt1;html-type:text,name:opt2;html-type:text,name:opt3;html-type:text,name:opt4;html-type:text,name:correct_ans;html-type:select,name:audio_url;html-type:text,name:video_url;html-type:text,name:ques_cat_id;html-type:select,name:show_before_ques;html-type:checkbox;options:1,name:show_with_ques;html-type:checkbox;options:1,name:show_after_ques;html-type:checkbox;options:1,name:ques_time;html-type:text,name:aud_start_time;html-type:text,name:aud_end_time;html-type:text,name:aud_end_time;html-type:text,name:aud_max_time_limit;html-type:text,name:video_start_time;html-type:text,name:video_end_time;html-type:text,name:video_max_time_limit;html-type:text,name:proof_link;html-type:text,created_at,updated_at" --force
php artisan create:resources Question --force --template-name=ajaxified

php artisan resource-file:create Tags --fields="id,name,name:published;html-type:select;options:true|false,created_at,updated_at" --force
php artisan create:resources Tag --force --template-name=ajaxified

php artisan resource-file:create QuizLanguages --fields="id,title,name:code;html-type:text,name:flag;html-type:file,name:status;html-type:select;options:active|inactive,created_at,updated_at" --force
php artisan create:resources QuizLanguage --force --template-name=ajaxified

php artisan resource-file:create Themes --fields="id,title,name:description;html-type:textarea;name:web_bg_image;html-type:file,name:mob_bg_images;html-type:file,name:status;html-type:select;options:active|inactive,created_at,updated_at" --force
php artisan create:resources Theme --force --template-name=ajaxified

php artisan resource-file:create ThemeCategories --fields="id,title,name:status;html-type:select;options:active|inactive,created_at,updated_at" --force
php artisan create:resources ThemeCategory --force --template-name=ajaxified

php artisan resource-file:create TicketCategories --fields="id,name,created_at,updated_at" --force
php artisan create:resources TicketCategory --force --template-name=ajaxified
*/