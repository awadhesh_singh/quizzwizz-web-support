<div class="container">
<div class="{{ $errors->has('title') ? 'has-error' : '' }}">

    <div class="col-md-8">
        <label for="name" class="control-label">Name</label>
        <input class="form-control js-title" name="name" type="text" id="name" value="{{ isset($quizCategory)?$quizCategory->name:'' }}" minlength="1" maxlength="255" placeholder="Enter name here...">
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>

{{--<div class="{{ $errors->has('alias') ? 'has-error' : '' }}">--}}
    {{--<div class="col-md-8  {{ $errors->has('alias') ? 'has-error' : '' }}">--}}
        {{--<label for="alias" class="control-label">Alias</label>--}}
        {{--<input class="form-control js-alias" name="alias" type="text" id="alias" value="{{ old('alias', optional($quizCategory)->alias) }}" minlength="1" maxlength="255" placeholder="Enter alias here...">--}}
        {{--{!! $errors->first('alias', '<p class="help-block">:message</p>') !!}--}}
    {{--</div>--}}
{{--</div>--}}

{{--<div class="row {{ $errors->has('published') ? 'has-error' : '' }}">--}}

    {{--<div class="col-md-8  ml-0 row {{ $errors->has('published') ? 'has-error' : '' }} ">--}}
        {{--<label for="published" class="control-label">Published</label>--}}
        {{--<select class="form-control" id="published" name="published">--}}
        	    {{--<option value="" style="display: none;" {{ old('published', optional($quizCategory)->published ?: '') == '' ? 'selected' : '' }} disabled selected>Select published</option>--}}
        	{{--@foreach (['true' => 'True',--}}
{{--'false' => 'False'] as $key => $text)--}}
			    {{--<option value="{{ $key }}" {{ old('published', optional($quizCategory)->published) == $key ? 'selected' : '' }}>--}}
			    	{{--{{ $text }}--}}
			    {{--</option>--}}
			{{--@endforeach--}}
        {{--</select>--}}
        {{----}}
        {{--{!! $errors->first('published', '<p class="help-block">:message</p>') !!}--}}
    {{--</div>--}}
</div>
</div>
