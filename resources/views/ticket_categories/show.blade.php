@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row ">
            <div class="col-sm-8">
                <div class="panel panel-default" >
                    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($quizCategory->name) ? $quizCategory->name : 'Ticket Category' }}</h4>
        </span>

                        <div class="pull-right">

                            <form method="POST" action="{!! route('ticket_categories.ticket_category.destroy', $quizCategory->id) !!}" accept-charset="UTF-8">
                                <input name="_method" value="DELETE" type="hidden">
                                {{ csrf_field() }}
                                <div class="btn-group btn-group-sm" role="group">
                                    <a href="{{ route('ticket_categories.ticket_category.index') }}" class="btn btn-primary" title="Show All Ticket Category">
                                        <span class="fa fa-list" aria-hidden="true"></span>
                                        {{--<span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>--}}
                                    </a>

                                    {{--<a href="{{ route('quiz_categories.quiz_category.create') }}" class="btn btn-success" title="Create New Quiz Category">--}}
                                        {{--<span class="fa fa-plus" aria-hidden="true"></span>--}}
                                        {{--<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>--}}
                                    {{--</a>--}}

                                    {{--<a href="{{ route('quiz_categories.quiz_category.edit', $quizCategory->id ) }}" class="btn btn-primary" title="Edit Quiz Category">--}}
                                        {{--<span class="fa fa-pencil" aria-hidden="true"></span>--}}
                                        {{--<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>--}}
                                    {{--</a>--}}

                                    {{--<button type="submit" class="btn btn-danger" title="Delete Quiz Category" onclick="return confirm(&quot;Delete Quiz Category??&quot;)">--}}
                                        {{--<span class="fa fa-trash" aria-hidden="true"></span>--}}
                                        {{--<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>--}}
                                    {{--</button>--}}
                                </div>
                            </form>

                        </div>

                    </div>

                    <div class="panel-body mx-3 mb-3">
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <dt>Name</dt>
                                <dd>{{ $quizCategory->name }}</dd>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <dt>Created At</dt>
                                <dd>{{ $quizCategory->created_at }}</dd>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <dt>Updated At</dt>
                                <dd>{{ $quizCategory->updated_at }}</dd>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection