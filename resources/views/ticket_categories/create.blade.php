@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row ">
            <div class="col-sm-8">
    <div class="panel panel-default py-3">

        <div class="panel-heading clearfix">
            
            <span class="pull-left">
                <h4>Create New Ticket Category</h4>
            </span>

            <div class="btn-group btn-group-sm pull-right" role="group">
            <a href="{{ route('ticket_categories.ticket_category.index') }}" title="Show All Ticket Category">
                                <button class="btn btn-labeled btn-green mb-2" type="button">
                                       <span class="btn-label"><i class="fa fa-list"></i>
                                       </span>Show All TC</button>
                            </a>
            </div>

        </div>

        <div class="panel-body">
        
            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            <form method="POST" action="{{ route('ticket_categories.ticket_category.store') }}" accept-charset="UTF-8" id="create_quiz_category_form" name="create_ticket_category_form" class="form-horizontal">
            {{ csrf_field() }}
            @include ('ticket_categories.form', [
                                        'quizCategory' => null,
                                      ])

                <div class="row ml-1">
                    <div class="col-md-6 col-sm-6">
                        <input class="btn btn-primary" type="submit" value="Add">
                    </div>
                </div>

            </form>

        </div>
    </div>
    </div>
        </div>
    </div>
@endsection


