@extends('layouts.app')

@section('content')
    <div class="row ">
        <div class="col-sm-8">
    <div class="panel panel-default ">
  
        <div class="panel-heading clearfix mt-4">

            <div class="pull-left">
                <h4 class="mx-5 ">{{ !empty($quizCategory->name) ? $quizCategory->name : 'Ticket Category' }}</h4>
            </div>
            <div class="btn-group btn-group-sm pull-right" role="group">

                <a href="{{ route('ticket_categories.ticket_category.index') }}" class="btn btn-primary" title="Show All Ticket Category">
                    <span class="fa fa-list" aria-hidden="true"></span>
                </a>

                <a href="{{ route('ticket_categories.ticket_category.create') }}" class="btn btn-success" title="Create New Ticket Category">
                    <span class="fa fa-plus" aria-hidden="true"></span>
                </a>

            </div>
        </div>

        <div class="panel-body mx-5">

            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            <form method="POST" action="{{ route('ticket_categories.ticket_category.update', $quizCategory->id) }}" id="edit_quiz_category_form" name="edit_quiz_category_form" accept-charset="UTF-8" class="form-horizontal">
            {{ csrf_field() }}
            <input name="_method" type="hidden" value="PUT">
            @include ('ticket_categories.form', [
                                        'quizCategory' => $quizCategory,
                                      ])

                <div class="row">
                    <div class="col-md-offset-2 col-md-6 mb-3 ml-3">
                        <input class="btn btn-primary" type="submit" value="Update">
                    </div>
                </div>
            </form>

        </div>
    </div>
  </div>
    </div>
@endsection