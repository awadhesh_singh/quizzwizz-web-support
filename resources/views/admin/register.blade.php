<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="Bootstrap Admin App + jQuery">
    <meta name="keywords" content="app, responsive, jquery, bootstrap, dashboard, admin">
    <title>QuizzWizz - Support</title>
    <!-- =============== VENDOR STYLES ===============-->
    <!-- FONT AWESOME-->
    <link rel="stylesheet" href="{{ url('theme-angle/vendor/font-awesome/css/font-awesome.css') }}">
    <!-- SIMPLE LINE ICONS-->
    <link rel="stylesheet" href="{{ url('theme-angle/vendor/simple-line-icons/css/simple-line-icons.css') }}">
    <!-- =============== BOOTSTRAP STYLES ===============-->
    <link rel="stylesheet" href="{{ url('theme-angle/css/bootstrap.css') }}" id="bscss">
    <!-- =============== APP STYLES ===============-->
    <link rel="stylesheet" href="{{ url('theme-angle/css/app.css') }}" id="maincss">
    <link id="autoloaded-stylesheet" rel="stylesheet" href="{{ url('theme-angle/css/theme-sk.css?v=1')}}">
    <link rel="icon" type="image/x-icon" href="{{ asset('quizzwizz/images/logo.png') }}">
</head>

<body>
<div class="wrapper">
    <div class="block-center wd-xl" style="margin-top: 8rem">
        <!-- START card-->
        <div class="card card-flat login-card">
            <div class="card-header text-center bg-dark">
                <a href="javascript:void(0)">
                    <img style="    width: 160px; position: relative;top: -50px;" class="block-center rounded" src="{{asset('quizzwizz/images/logo.png') }}" alt="QuizzWizz Logo">
                </a>
            </div>
            <div class="card-body">
                <p class="text-center py-2">Register New User.</p>
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name" class="col-md-12 control-label">Name</label>

                        <div class="col-md-12">
                            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">

                            @if ($errors->has('name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-md-12 control-label">E-Mail Address</label>

                        <div class="col-md-12">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="col-md-12 control-label">Password</label>

                        <div class="col-md-12">
                            <input id="password" type="password" class="form-control" name="password">

                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        <label for="password-confirm" class="col-md-12 control-label">Confirm Password</label>

                        <div class="col-md-12">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation">

                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4" style="display:flex">
                            <button type="submit" class="btn btn-primary">
                                <i class="fa fa-btn fa-user"></i> Register
                            </button>
                            <a href="{{url('login')}}" class="btn btn-primary" style="float:right; margin-left: 97px">
                                <i class="fa fa-btn fa-user"></i> Login
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END card-->
        <div class="p-3 text-center" style="color: #ffffff;">
            <span class="mr-2">&copy;</span><span>{{ date('Y') }}</span><span class="mr-2"> -</span><span>QuizzWizz</span>
            <br>
        </div>
    </div>
</div>
<!-- =============== VENDOR SCRIPTS ===============-->
<!-- MODERNIZR-->
<script src="{{ url('theme-angle/vendor/modernizr/modernizr.custom.js') }}"></script>
<!-- JQUERY-->
<script src="{{ url('theme-angle/vendor/jquery/dist/jquery.js') }}"></script>
<!-- BOOTSTRAP-->
<script src="{{ url('theme-angle/vendor/bootstrap/dist/js/bootstrap.js') }}"></script>
<!-- STORAGE API-->
<script src="{{ url('theme-angle/vendor/js-storage/js.storage.js') }}"></script>
<!-- PARSLEY-->
<script src="{{ url('theme-angle/vendor/parsleyjs/dist/parsley.js')}}"></script>
<!-- =============== APP SCRIPTS ===============-->
<script src="{{ url('theme-angle/js/app.js') }}"></script>
</body>

</html>