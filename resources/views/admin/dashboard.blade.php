@extends('layouts.app')
@section('content')
    <div class="content-wrapper">
        <div class="content-heading">
            <strong>Quiz Wizz Support
                <small data-localize="dashboard.WELCOME"></small>
            </strong>
        </div>
        <div class="row">
            @if($user->is_admin)
                <div class="col-xl-3 col-md-6">
                    <a href="">
                        <div class="card flex-row align-items-center align-items-stretch border-0">
                            <div class="col-4 d-flex align-items-center bg-primary-dark justify-content-center rounded-left">
                                <em class="icon-briefcase fa-3x"></em>
                            </div>
                            <div class="col-8 py-3 bg-primary rounded-right">
                                <div class="h2 mt-0">{{$categoryCount}}</div>
                                <div class="text-uppercase">Ticket Categories</div>
                            </div>
                        </div>
                    </a>
                </div>
            @endif
            <div class="col-xl-3 col-md-6">
                <a href="{{ url('customer/my_tickets') }}">
                    <div class="card flex-row align-items-center align-items-stretch border-0">
                        <div class="col-4 d-flex align-items-center bg-purple-dark justify-content-center rounded-left">
                            <em class="icon-doc fa-3x"></em>
                        </div>
                        <div class="col-8 py-3 bg-purple rounded-right">
                            <div class="h2 mt-0">{{$totalTickets}}</div>
                            <div class="text-uppercase">Total Tickets</div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-xl-3 col-md-6">
                <a href="{{ url('customer/my_tickets?status=Closed') }}">
                    <div class="card flex-row align-items-center align-items-stretch border-0">
                        <div class="col-4 d-flex align-items-center bg-primary-dark justify-content-center rounded-left">
                            <em class="icon-doc fa-3x"></em>
                        </div>
                        <div class="col-8 py-3 bg-primary rounded-right">
                            <div class="h2 mt-0">{{$closedTickets}}</div>
                            <div class="text-uppercase">Closed Tickets</div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-xl-3 col-md-6">
                <a href="{{ url('customer/my_tickets?status=Open') }}">
                    <div class="card flex-row align-items-center align-items-stretch border-0">
                        <div class="col-4 d-flex align-items-center bg-purple-dark justify-content-center rounded-left">
                            <em class="icon-doc fa-3x"></em>
                        </div>
                        <div class="col-8 py-3 bg-purple rounded-right">
                            <div class="h2 mt-0">{{$openTickets}}</div>
                            <div class="text-uppercase">Open Tickets</div>
                        </div>
                    </div>
                </a>
            </div>
        </div> <br><br>
        <div class="content-heading">
            <div class="row col-md-12 col-sm-12 col-lg-12">
                <div class="col-md-8 col-sm-8 col-lg-8">
                    <strong>Open Tickets
                        <small data-localize="dashboard.WELCOME"></small>
                    </strong>
                </div>
                <div class="col-md-4 col-sm-4 col-lg-4">
                    <div class="btn-group btn-group-sm pull-right" role="group">
                        <a href="{{ url('customer/my_tickets') }}" title="Show all Tickets">
                            <button class="btn btn-labeled btn-green mb-2" type="button">
                                       <span class="btn-label"><i class="fa fa-list"></i>
                                       </span>Show all Tickets</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row col-sm-12 col-md-12 col-lg-12">
            @if ($tickets->isEmpty())
                <p>There are currently no tickets.</p>
            @else
                <table id="tickets_data" class="table">
                    <thead>
                    <tr>
                        <th>Category</th>
                        <th>Title</th>
                        <th>Status</th>
                        <th>Owner</th>
                        <th>Last Updated</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($tickets as $ticket)
                        <tr>
                            <td>
                                @foreach ($categories as $category)
                                    @if ($category->id === $ticket->category_id)
                                        {{ $category->name }}
                                    @endif
                                @endforeach
                            </td>
                            <td>
                                <a href="{{ url('admin/tickets/'. $ticket->ticket_id) }}">
                                    #{{ $ticket->ticket_id }} - {{ $ticket->title }}
                                </a>
                            </td>
                            <td>
                                @if ($ticket->status === 'Open')
                                    <span class="label label-success">{{ $ticket->status }}</span>
                                @else
                                    <span class="label label-danger">{{ $ticket->status }}</span>
                                @endif
                            </td>
                            <td>{{ ucfirst($ticket->user->name) }}</td>
                            <td>{{ $ticket->updated_at }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
@endsection