@extends('layouts.app')
@section('title', $ticket->title)
@section('content')
    <div class="row custom-style">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    #{{ $ticket->ticket_id }} - {{ $ticket->title }}
                    <div class="btn-group btn-group-sm pull-right" role="group" style="margin-top: -2px;">
                        <a href="{{ url('customer/my_tickets') }}" title="Show all Tickets">
                            <button class="btn btn-labeled btn-green mb-2" type="button">
                                       <span class="btn-label"><i class="fa fa-list"></i>
                                       </span>Show all Tickets</button>
                        </a>
                    </div><br>
                </div>

                <div class="panel-body"  >
                    @include('includes.flash')
                    <div class="comment-form">
                        <div style="margin: 16px !important;" class="ticket-info">
                            <p>{{ $ticket->message }}</p>
                            <p>Categry: {{ $category->name }}</p>
                            <p>
                                @if ($ticket->status === 'Open')
                                    Status: <span class="label label-success" >{{ $ticket->status }}</span>
                                @else
                                    Status: <span class="label label-danger">{{ $ticket->status }}</span>
                                @endif
                            </p>
                            <p>Created on: {{ $ticket->created_at->diffForHumans() }}</p>
                        </div>
                        <div style="margin: 16px !important;" class="comments" >
                            @foreach ($comments as $comment)
                                <div  class="panel panel-@if($ticket->user->id == $comment->user_id){{"default"}}@else{{"success"}}@endif">
                                    <div class="panel panel-heading" >
                                        {{ $comment->user->name }}
                                        <span class="pull-right">{{ $comment->created_at->format('Y-m-d') }}</span>
                                    </div>

                                    <div class="panel panel-body" >
                                        {{ $comment->comment }}
                                    </div>
                                    <div class="panel panel-body" >
                                        @foreach($comment->attachments as $at)
                                            <a href="{{env('APP_URL')}}attachment/{{$at->attachment}}" target="_blank"><span style="padding-left: 10px"><i class="fa fa-paperclip"></i> {{ $at->attachment }} </span></a>
                                        @endforeach
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        @if($ticket->status =='Open')
                            <form action="{{ url('comment') }}" method="POST" class="form" enctype="multipart/form-data">
                                {!! csrf_field() !!}
                                <input type="hidden" name="ticket_id" value="{{ $ticket->id }}">
                                <div class="form-group{{ $errors->has('comment') ? ' has-error' : '' }}">
                                    <textarea rows="10" id="comment" class="form-control" name="comment"></textarea>
                                    @if ($errors->has('comment'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('comment') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('comment') ? ' has-error' : '' }}">
                                    <input id="attachment" type="file" multiple class="form-control" name="attachment[]"></input>

                                    @if ($errors->has('attachment'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('attachment') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        @endif
                    </div>
                </div>
            </div>
        </div>
@endsection