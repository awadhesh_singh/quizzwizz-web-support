@extends('layouts.app')
@section('title', 'My Tickets')
@section('content')
	<div class="row">
		<div class="col-md-12 col-md-offset-1">
	        <div class="panel panel-default">
	        	<div class="panel-heading">
					<div style="margin: 0 !important; padding: 0 !important;" class="row col-sm-12 col-md-12 col-lg-12">
						<div class="col-sm-8 col-md-8 col-lg-8">
							<i class="fa fa-ticket"> My Tickets</i>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-2">
							<select id="status" class="form-control" name="status" onchange="change_url(this.value);">
								@if($status=="Open")
									<option value="{{$status}}" selected="selected">{{$status}}</option>
									<option value="">All</option>
									<option value="Closed">Closed</option>
								@elseif($status=="Closed")
									<option value="{{$status}}" selected="selected">{{$status}}</option>
									<option value="">All</option>
									<option value="Open">Open</option>
								@else
									<option value="">All</option>
									<option value="Open">Open</option>
									<option value="Closed">Closed</option>
								@endif
							</select>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-2">
							<a href="{{ url('customer/new_ticket') }}">
								<button class="btn btn-labeled btn-green mb-2" type="button">
                           <span class="btn-label"><i class="fa fa-plus"></i>
                           </span>Add New</button>
							</a>
						</div>
					</div>
	        	</div>
	        	<div class="col-sm-12 col-md-12 col-lg-12">
		        		<table class="table table-striped full-width" id="datatable-tickets">
		        			<thead>
		        				<tr>
		        					<th>Category</th>
		        					<th>Title</th>
		        					<th>Status</th>
		        					<th>Last Updated</th>
		        				</tr>
		        			</thead>
		        			<tbody>
							@if(count($tickets))
		        			@foreach ($tickets as $ticket)
								<tr>
		        					<td>
		        					@foreach ($categories as $category)
		        						@if ($category->id ==$ticket->category_id)
											{{ $category->name }}
		        						@endif
		        					@endforeach
		        					</td>
		        					<td>
		        						<a href="{{ url('customer/tickets/'. $ticket->ticket_id) }}">
		        							#{{ $ticket->ticket_id }} - {{ $ticket->title }}
		        						</a>
		        					</td>
		        					<td>
		        					@if ($ticket->status === 'Open')
		        						<span class="label label-success">{{ $ticket->status }}</span>
		        					@else
		        						<span class="label label-danger">{{ $ticket->status }}</span>
		        					@endif
		        					</td>
		        					<td>{{ $ticket->updated_at }}</td>
		        				</tr>
		        			@endforeach
							@endif
		        			</tbody>
		        		</table>
	        	</div>
	        </div>
	    </div>
	</div>
@endsection
@section('styles')
	<!-- Datatables-->
	<link rel="stylesheet" href="{{url('theme-angle')}}/vendor/datatables.net-bs4/css/dataTables.bootstrap4.css">
@endsection
@section('javascript')
	<script src="{{url('theme-angle')}}/vendor/datatables.net/js/jquery.dataTables.js"></script>
	<script src="{{url('theme-angle')}}/vendor/datatables.net-bs4/js/dataTables.bootstrap4.js"></script>

	<script src="{{url('theme-angle')}}/vendor/datatables.net-buttons/js/dataTables.buttons.js"></script>
	<script src="{{url('theme-angle')}}/vendor/datatables.net-buttons-bs/js/buttons.bootstrap.js"></script>
	<script src="{{url('theme-angle')}}/vendor/datatables.net-buttons/js/buttons.colVis.js"></script>
	<script src="{{url('theme-angle')}}/vendor/datatables.net-buttons/js/buttons.flash.js"></script>
	<script src="{{url('theme-angle')}}/vendor/datatables.net-buttons/js/buttons.html5.js"></script>
	<script src="{{url('theme-angle')}}/vendor/datatables.net-buttons/js/buttons.print.js"></script>
	<script src="{{url('theme-angle')}}/vendor/datatables.net-keytable/js/dataTables.keyTable.js"></script>
	<script src="{{url('theme-angle')}}/vendor/datatables.net-responsive/js/dataTables.responsive.js"></script>
	<script src="{{url('theme-angle')}}/vendor/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
	<script src="{{url('theme-angle')}}/vendor/jszip/dist/jszip.js"></script>
	<script src="{{url('theme-angle')}}/vendor/pdfmake/build/pdfmake.js"></script>
	<script src="{{url('theme-angle')}}/vendor/pdfmake/build/vfs_fonts.js"></script>

	<script>
        function change_url(val) {
            if(val == '') {
                window.location.href = "{{URL::to('customer/my_tickets')}}";
            } else {
                window.location.href = "{{URL::to('customer/my_tickets?status=')}}" + val;
            }
        }
        $('#datatable-tickets').DataTable({
            'paging': true, // Table pagination
            'ordering': true, // Column ordering
            'info': true, // Bottom left status text
            responsive: true,
            // Text translation options
            // Note the required keywords between underscores (e.g _MENU_)
            oLanguage: {
                sSearch: 'Search all columns:',
                sLengthMenu: '_MENU_ records per page',
                info: 'Showing page _PAGE_ of _PAGES_',
                zeroRecords: 'Nothing found - sorry',
                infoEmpty: 'No records available',
                infoFiltered: '(filtered from _MAX_ total records)',
                oPaginate: {
                    sNext: '<em class="fa fa-caret-right"></em>',
                    sPrevious: '<em class="fa fa-caret-left"></em>'
                }
            },
            // Datatable Buttons setup
            dom: 'Bfrtip',
            buttons: [
                { extend: 'copy', className: 'btn-green' },
                { extend: 'csv', className: 'btn-green' },
                { extend: 'excel', className: 'btn-green', title: 'XLS-File' },
                { extend: 'pdf', className: 'btn-green', title: $('title').text() },
                { extend: 'print', className: 'btn-green' }
            ]
        });

	</script>
@endsection