<div class="aside-inner">
    <nav class="sidebar" data-sidebar-anyclick-close="">
        <!-- START sidebar nav-->
        <ul class="sidebar-nav">
            <!-- START user info-->
            <li class="has-user-block">
                <div class="collapse" id="user-block">
                    <div class="item user-block">
                        <!-- User picture-->
                        <div class="user-block-picture">
                            <div class="user-block-status">
                                <img class="img-thumbnail rounded-circle" src="{{asset('theme-angle/img/admin.png')}}" alt="Avatar" width="60" height="60">
                                <div class="circle bg-success circle-lg"></div>
                            </div>
                        </div>

                        <div class="user-block-info">
                            <span class="user-block-name">Hello, {{\Auth::user()->name  }}</span>
                        </div>
                    </div>
                </div>
            </li>
            <!-- END user info-->
            <!-- Iterates over all sidebar items-->
            <li class="nav-heading ">
                <span data-localize="sidebar.heading.HEADER">Main Navigation</span>
            </li>
            <li class="{{ request()->is('customer/dashboard') ? 'active' : '' }}">
                <a href="{{url('customer/dashboard')}}" title="Dashboard" >
                    {{-- <div class="float-right badge badge-success">3</div>--}}
                    <em class="icon-speedometer"></em>
                    <span>Dashboard</span>
                </a>
            </li>


            @if(\Auth::user()->is_admin)
            <li class=" ">
                <a href="#masters" title="Masters" data-toggle="collapse">
                    <div class="float-right badge bg-primary">1</div>
                    <em class="fa fa-database"></em>
                    <span data-localize="sidebar.nav.DASHBOARD">Masters</span>
                </a>
                <ul class="nav sidebar-subnav collapse" id="masters">
                    <li class="sidebar-subnav-header">Masters</li>
                    <li class="">
                        <a href="{{ route('ticket_categories.ticket_category.index') }}" title="Ticket Categories">
                            <em class="fa fa-newspaper-o"></em>
                            <span >Ticket Categories</span>
                        </a>
                    </li>
                </ul>
            </li>
                <li class="{{ request()->is('admin/tickets') ? 'active' : '' }}">
                    <a href="{{ url('admin/tickets') }}" title="Tickets">
                        <em class="fa fa-ticket"></em>
                        <span >All Ticket</span>
                    </a>
                </li>
            @else
            <li class="{{ request()->is('customer/my_tickets') ? 'active' : '' }}">
                <a href="{{ url('customer/my_tickets') }}" title="Tickets">
                    <em class="fa fa-ticket"></em>
                    <span >My Ticket</span>
                </a>
            </li>
            @endif
        </ul>
        <!-- END sidebar nav-->
    </nav>
</div>